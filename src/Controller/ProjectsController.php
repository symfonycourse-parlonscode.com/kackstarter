<?php

namespace App\Controller;

use App\Entity\Project;
use App\Repository\ProjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProjectsController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @Route("/projects", name="projects.index")
     */
    public function index(ProjectRepository $repo): Response
    {
        $projects = $repo->findAll();
        return $this->render('projects/index.html.twig', compact('projects'));
    }

    /**
     * @Route("/projects/{id<[1-9]*>}", name="projects.show")
     */
    public function show(Project $project): Response
    {
        return $this->render('projects/show.html.twig', compact('project'));
    }
}
