<?php

namespace App\Twig;

use DateTime;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use InvalidArgumentException;
use Knp\Bundle\TimeBundle\KnpTimeBundle;
use Symfony\Component\Validator\Constraints\Date;

class AppExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
           // new TwigFilter('filter_name', [$this, 'doSomething']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('pluralize', [$this, 'pluralize']),
            new TwigFunction('format_date', [$this, 'formatDate']),

        ];
    }

    public function pluralize(int $count, string $singular, String $plural = null): string
    {
        if (!is_numeric($count)) {
            throw new InvalidArgumentException('$count must be a numeric value');
        }

        $plural = $plural ?? $singular . 's';

        switch ($count) {
            case 1:
                $string = $singular;
                break;
            
            default:
                $string = $plural;
                break;
        }
        return sprintf("%d %s", $count, $string);
    }

    public function formatDate(DateTime $expireOn)
    {
        return date_format($expireOn, 'Y-m-d H:i:s');
    }
}
